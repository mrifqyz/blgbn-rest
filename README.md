## Coverage & Pipeline  

## Master
[![pipeline status](https://gitlab.com/adpro-15/balgebun-bot/badges/master/pipeline.svg)](https://gitlab.com/adpro-15/balgebun-bot/-/commits/master)
[![coverage report](https://gitlab.com/adpro-15/balgebun-bot/badges/master/coverage.svg)](https://gitlab.com/adpro-15/balgebun-bot/-/commits/master)

###  Database Branch
[![pipeline status](https://gitlab.com/adpro-15/balgebun-bot/badges/database/pipeline.svg)](https://gitlab.com/adpro-15/balgebun-bot/-/commits/database)
[![coverage report](https://gitlab.com/adpro-15/balgebun-bot/badges/database/coverage.svg)](https://gitlab.com/adpro-15/balgebun-bot/-/commits/database)

### UI/UX Branch

### Purchase Branch
[![pipeline status](https://gitlab.com/adpro-15/balgebun-bot/badges/purchase/pipeline.svg)](https://gitlab.com/adpro-15/balgebun-bot/-/commits/purchase)
[![coverage report](https://gitlab.com/adpro-15/balgebun-bot/badges/purchase/coverage.svg)](https://gitlab.com/adpro-15/balgebun-bot/-/commits/purchase)

### Promo Branch

### Menu Branch
``TODO NEXT``
